# -*- coding: utf-8 -*
import os  
from time import sleep
# [Content] XML Footer Text

def test_hasHomePageNode():
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('首頁') != -1

# [Behavior] Tap the coordinate on the screen
def test_tapSidebar():
	os.system('adb shell input tap 100 100')

# 1. [Content] Side Bar Text
def test_Sidebartext():
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('查看商品分類') != -1
	assert xmlString.find('查訂單/退訂退款') != -1
	assert xmlString.find('追蹤/買過/看過清單') != -1
	assert xmlString.find('智慧標籤') != -1
	assert xmlString.find('PChome 旅遊') != -1
	assert xmlString.find('線上手機回收') != -1
	assert xmlString.find('給24h購物APP評分') != -1


# 2. [Screenshot] Side Bar Text
def test_screenshot_Sidebartext():
	os.system('adb shell screencap -p /sdcard/screen.png && adb pull /sdcard/screen.png ./Sidebartext.png')

# 3. [Context] Categories
def test_Categories():
	os.system('adb shell input keyevent 4')
	sleep(2)
	os.system('adb shell input swipe 700 700 250 250')
	sleep(2)
	os.system('adb shell input tap 1002 1086')
	sleep(2)
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert  xmlString.find('3C') != -1
	assert  xmlString.find('周邊') != -1
	assert  xmlString.find('NB') != -1

# 4. [Screenshot] Categories
def test_screenshot_Categories():
	os.system('adb shell screencap -p /sdcard/screen.png && adb pull /sdcard/screen.png ./Categories.png')
\
# 5. [Context] Categories page
def test_Categoriespage():
	os.system('adb shell input tap 1018 281')
	sleep(2)
	os.system('adb shell input tap 324 1713')
	sleep(2)
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert  xmlString.find('3C') != -1
	assert  xmlString.find('周邊') != -1
	assert  xmlString.find('NB') != -1
# 6. [Screenshot] Categories page
def test_screenshot_Categoriespage():
	os.system('adb shell screencap -p /sdcard/screen.png && adb pull /sdcard/screen.png ./Categories_page.png')

# 7. [Behavior] Search item “switch”
def test_Search():
	os.system('adb shell input tap 342 128')
	sleep(5)
	os.system('adb shell input text "switch"')
	sleep(5)
	os.system('adb shell input keyevent 66')
	sleep(5)
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert  xmlString.find('Nintendo Switch') != -1

# 8. [Behavior] Follow an item and it should be add to the list
def test_Follow():
	os.system('adb shell input tap 558 523')
	sleep(5)
	os.system('adb shell input tap 100 1695')
	sleep(5)
	os.system('adb shell input keyevent 4')
	sleep(5)
	os.system('adb shell input tap 106 1704')
	sleep(5)
	os.system('adb shell input tap 100 100')
	sleep(5)
	os.system('adb shell input tap 424 887')
	sleep(5)
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert  xmlString.find('Nintendo Switch') != -1

# 9. [Behavior] Navigate tto the detail of item
def test_Navigate():
	os.system('adb shell input tap 383 1100')
	sleep(3)
	os.system('adb shell input swipe 700 700 250 250')
	sleep(3)
	os.system('adb shell input tap 533 125')
	sleep(3)
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert  xmlString.find('健身環') != -1

# 10. [Screenshot] Disconnetion Screen
def test_Disconnect():
	os.system('adb shell input swipe 512 40 512 1000')
	sleep(1)
	os.system('adb shell input tap 83 299')
	os.system('adb shell input swipe 512 1000 512 40')
	sleep(2)
	os.system('adb shell screencap -p /sdcard/screen.png && adb pull /sdcard/screen.png ./Disconnect.png')
